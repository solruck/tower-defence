﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Castle : MonoBehaviour {
    public float Health = 5000;
    public HealthBar HealthBar;
    public GameUI GameUI;

    public void Hit()
    {
        Health -= 5;
        HealthBar.Set(5000, Health);
    }
    private void Update()
    {
        if(Health <= 0)
        {
            GameUI.YouLose();
        }
    }
}
