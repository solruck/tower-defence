﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameUI : MonoBehaviour {
    public GameObject GameOverGO;
    public GameObject YouWinGO;
    public GameObject Player;

    public void YouWin()
    {
        YouWinGO.SetActive(true);
        Player.SetActive(false);
    }
    public void YouLose()
    {
        GameOverGO.SetActive(true);
        Player.SetActive(false);
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
