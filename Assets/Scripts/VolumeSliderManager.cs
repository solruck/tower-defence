﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VolumeSliderManager : MonoBehaviour
{
    public Slider VolumeSlider;
    public AudioSource AudioSource;
    public bool GonnaChangeBackToMenu = false;
    public Text VolumeText;
    public GameObject AudioPlayer;

    private void Start()
    {
        AudioSource = Instantiate(AudioPlayer).GetComponent<AudioSource>();
        DontDestroyOnLoad(AudioSource.gameObject);
        DontDestroyOnLoad(this);
    }
    public void OnSoundSliderChange()
    {
        AudioSource.volume = VolumeSlider.value;
        VolumeText.text = "Volume: " +  VolumeSlider.value * 100;
    }
    private void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            VolumeSlider.gameObject.transform.position = new Vector3(220, 10, VolumeSlider.gameObject.transform.position.z);
            GonnaChangeBackToMenu = true;
        }
        if (GonnaChangeBackToMenu && SceneManager.GetActiveScene().buildIndex == 0)
        {
            AudioSource.Stop();
            Destroy(this);
            Destroy(AudioSource.gameObject);
            Destroy(VolumeText.gameObject);
            Destroy(VolumeSlider.gameObject);
        }
    }
}
