﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateUI : MonoBehaviour {
    public Text CurrentResources;
    public Text CurrentWorkers;
    public Text CurrentFighters;
    public Text CurrentPopulation;
    public int CurrentResourcesAmount;
    public int CurrentWorkersAmount;
    public int CurrentFightersAmount;
    public GameUI GameUI;

    private int Population() {
        return CurrentWorkersAmount + CurrentFightersAmount;
    }

    // Use this for initialization
    void Start () {
        CurrentResourcesAmount = 20;
        CurrentWorkersAmount = 0;
        CurrentFightersAmount = 0;
    }
    /// <summary>s
    /// Removes the population by a given amount. Note: updates are already done for the UI in this function.
    /// </summary>
    /// <param name="amount">The amount of people to remove. Note they must all be the same type of minnion.</param>
    /// <param name="typeOfMinion">The type of minnion you are removing(fighter/worker)</param>
    public void RemovePopulationBy(int amount, string typeOfMinion)
    {
        if(typeOfMinion == "fighter")
        {
            CurrentFightersAmount -= amount;
        }
        else if (typeOfMinion == "worker")
        {
            CurrentWorkersAmount -= amount;
        }
        UpdatePopulation();
        UpdateWorkers(0);
        UpdateFighters(0);
    }
    public void UseResources(int resourcesUsed)
    {

        CurrentResourcesAmount -= resourcesUsed;
        CurrentResources.text = "Resources: " + CurrentResourcesAmount;
    }
    public void UpdatePopulation()
    {
        CurrentPopulation.text = "Population: " + Population();
    }
	public void UpdateResources(int resourcesToAdd)
    {
        CurrentResourcesAmount += resourcesToAdd;
        CurrentResources.text = "Resources: " + CurrentResourcesAmount;
    }
    public void UpdateWorkers(int workers)
    {
        CurrentWorkersAmount += workers;
        CurrentWorkers.text = "Workers: " + CurrentWorkersAmount;
        UpdatePopulation();
    }
    public void UpdateFighters(int fighters)
    {
        CurrentFightersAmount += fighters;
        CurrentFighters.text = "Fighters: " + CurrentFightersAmount;
        UpdatePopulation();
    }
    // Update is called once per frame
    void Update () {
		if(Population() <= 0 && CurrentResourcesAmount <= 0)
        {
            GameUI.YouLose();
        }
	}
}
