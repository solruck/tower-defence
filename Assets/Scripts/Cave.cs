﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cave : MonoBehaviour {
    public HealthBar HealthBar;
    public float Currenthealth = 10000;
    public GameUI GameUI;
    public void Hit()
    {
        Currenthealth -= 2;
        HealthBar.Set(10000, Currenthealth);
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Currenthealth <= 0)
        {
            Die();
        }
	}

    private void Die()
    {
        GameUI.YouWin();
    }
}
