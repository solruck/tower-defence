﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worker : MonoBehaviour {
    public float Speed;
    private float Health = 20;
    private float resourceSpeedPenalty = 0.5f;
    public bool AmIStoringAResource = false;
    public List<GameObject> All_Resources_For_Grabs = new List<GameObject>();
    public UpdateUI UpdateUI;
    private int ClosestMat = 0;
    private Vector3 home = new Vector3(99.5f, 1.5f, 0);
    public HealthBar HealthBar;
    // Use this for initialization
    void Start () {
        UpdateUI = GameObject.Find("UImanager").GetComponent<UpdateUI>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.GetComponent("Resource") as Resource) != null && !AmIStoringAResource)
        {
            AmIStoringAResource = true;
            SimplePool.Despawn(other.gameObject);
            ResourceStatus(true);
            ClosestMat = 0;
        }
    }

    public void ResourceStatus(bool TF)
    {
        GameObject BoxOnHead = transform.Find("Cube").gameObject;
        BoxOnHead.SetActive(TF);

    }

    public void Hit()
    {
        Health -= 5;
        AmIStoringAResource = false;
        HealthBar.Set(20, Health);
        if (Health <= 0)
        {
            Health = 20;
            AmIStoringAResource = false;
            UpdateUI.RemovePopulationBy(1, "worker");
            HealthBar.Set(20, 20);
            SimplePool.Despawn(this.gameObject);
        }
    }

    // Update is called once per frame
    void Update () {
        if (All_Resources_For_Grabs.Count == 0)
        {
            return;
        }
        if(AmIStoringAResource)
        {
            var speedDistance = Speed * Time.deltaTime* resourceSpeedPenalty;
            transform.position = Vector3.MoveTowards(transform.position, home, speedDistance);
            if (Vector3.Distance(transform.position,home) < 1)
            {

                UpdateUI.UpdateResources(10);
                ResourceStatus(false);
                AmIStoringAResource = false;
                ClosestMat = 0;
                Update();
            }
        }
        else
        {
            ClosestMat = 0;
            int CurrentLoops = 0;
            foreach (GameObject go in All_Resources_For_Grabs)
            {
                try
                {
                    if (Vector3.Distance(go.transform.position, transform.position) < Vector3.Distance(All_Resources_For_Grabs[ClosestMat].transform.position, transform.position))
                    {
                        ClosestMat = CurrentLoops;
                    }
                    CurrentLoops += 1;
                }
                catch (MissingReferenceException)
                {
                    return;
                }
            }

            var distance = Speed * Time.deltaTime;
            Vector3 PositionToGo = Vector3.MoveTowards(transform.position, All_Resources_For_Grabs[ClosestMat].transform.position, distance);
            transform.position = new Vector3(PositionToGo.x, 1.5f, PositionToGo.z);
        }
        
    }
}
