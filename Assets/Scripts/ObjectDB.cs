﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDB : MonoBehaviour {
    public List<GameObject> AllWorkerUnits = new List<GameObject>();
    public List<GameObject> All_Mats = new List<GameObject>();
    public GameObject ResourceGO;
    
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        UpdateAllMatsForWorkers();
        spawnResources();
    }
    public void AddWorkerToList(GameObject go)
    {
        AllWorkerUnits.Add(go);
        Worker WorkerScript = go.GetComponent<Worker>();
        WorkerScript.All_Resources_For_Grabs = All_Mats;
        WorkerScript.AmIStoringAResource = false;
    }
    public void UpdateAllMatsForWorkers()
    {
        if(AllWorkerUnits.Count != 0)
        {
            foreach(GameObject go in AllWorkerUnits)
            {
                Worker WorkerScript = go.GetComponent<Worker>();
                WorkerScript.All_Resources_For_Grabs = All_Mats;

            }
        }
    }

    private void spawnResources() {
        if (Random.value < 0.05)
        {
            int PosToSpawnNext = Random.Range(-100, 90);
            GameObject lastSpawned = SimplePool.Spawn(ResourceGO, new Vector3(PosToSpawnNext, 1, 0), Quaternion.identity);
            All_Mats.Add(lastSpawned);
            UpdateAllMatsForWorkers();
        }
    }
}
