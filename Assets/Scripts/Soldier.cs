﻿using UnityEngine;

public class Soldier : MonoBehaviour {
    // Update is called once per frames
    public float Speed;
    public GameObject flag;
    public float Health = 20;
    public UpdateUI UpdateUI;
    public HealthBar HealthBar;
    private bool CanIHit;
    public float CountDownTimer;
    private UnitState State = UnitState.move;
    public GameObject ToAttack;

    private enum UnitState
    {
        move,
        fight,
    }

    private void Start()
    {
        flag = GameObject.Find("flag");
        UpdateUI = GameObject.Find("UImanager").GetComponent<UpdateUI>();

    }
    public void Hit()
    {
        Health -= 10;
        HealthBar.Set(20, Health);
        if (Health <= 0)
        {
            UpdateUI.RemovePopulationBy(1, "fighter");
            Health = 20;
            HealthBar.Set(20, 20);
            State = UnitState.move;
            ToAttack = null;
            SimplePool.Despawn(this.gameObject);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<Enemy>() != null)
        {
            ToAttack = other.gameObject;
        }
        else if (other.gameObject.GetComponent<Cave>() != null)
        {
            ToAttack = other.gameObject;
        }
    }
    void Update () {

        if (State == UnitState.move) { Move();}
        else if (State == UnitState.fight) { Fight(); }
        else { Debug.LogError("unsupported state"); }

        CountDownTimer -= Time.deltaTime;
        if(CountDownTimer <= 0)
        {
            CanIHit = true;
            CountDownTimer = 0.5f;
        }
        
    }
    private void Fight()
    {
        if (Vector3.Distance(this.transform.position, ToAttack.transform.position) >= 3|| !ToAttack.activeSelf)
        {
            State = UnitState.move;
            ToAttack = null;
            return;
        }
        
        if (ToAttack.gameObject.GetComponent<Enemy>() != null && CanIHit)
        {
            ToAttack.gameObject.GetComponent<Enemy>().Hit();
        }
        else if (ToAttack.GetComponent<Cave>() != null && CanIHit)
        {
            ToAttack.GetComponent<Cave>().Hit();
        }
        CanIHit = false;
    }

    private void Move()
    {
        if (ToAttack != null)
        {
            State = UnitState.fight;
            return;
        }
        var distance = Speed * Time.deltaTime;
        Vector3 PositionToGo = Vector3.MoveTowards(transform.position, flag.transform.position, distance);
        transform.position = new Vector3(PositionToGo.x, 1.5f, PositionToGo.z);
    }

}