﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    public GameObject Red;
    public GameObject Green;

    public void Set(float max, float current)
    {
        var sr = Red.transform.localScale.x;
        var sg = current / max;
        if (current <= 0)
        {
            sg = 0f;
        }
        Green.transform.localScale = new Vector3(sg, Green.transform.localScale.y, Green.transform.localScale.z) ;
        Green.transform.localPosition = new Vector3((sr - sg) / 2, Green.transform.localPosition.y, Green.transform.localPosition.z);
    }

    void Update()
    {
        this.transform.LookAt(Camera.main.transform.position);
    }
}