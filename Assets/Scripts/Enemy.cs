﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    public float Speed;
    public bool InAFight;
    public float health = 20;
    //public GameObject closestSoldier;
    private float CoolDownTimer;
    private bool CanIHit = false;
    public HealthBar HealthBar;
    public GameObject ToAttack;
    //public Animator Animator;

    private UnitState State = UnitState.move;

    private enum UnitState
    {
        move,
        fight,
    }
	
    public void Hit()
    {
        health -= 5;
        HealthBar.Set(20, health);
        if (health <= 0)
        {
            health = 20;
            InAFight = false;
            HealthBar.Set(20, 20);
            State = UnitState.move;
            ToAttack = null;
            SimplePool.Despawn(this.gameObject);
        }
    }
	// Update is called once per frame
	void Update () {
        if (State == UnitState.move) { Move(); }
        else if (State == UnitState.fight) { Fight(); }
        else { Debug.LogError("Unsupported State"); }
        CoolDownTimer -= Time.deltaTime;
        if (CoolDownTimer <= 0f)
        {
            CanIHit = true;
            CoolDownTimer = 0.5f;
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<Soldier>() != null)
        {
            ToAttack = other.gameObject;
        }
        else if (other.gameObject.GetComponent<Worker>() != null) {
            ToAttack = other.gameObject;
        }
        else if (other.gameObject.GetComponent<Castle>() != null)
        {
            ToAttack = other.gameObject;

        }

    }
    
    private void Fight()
    {
        if (Vector3.Distance(this.transform.position, ToAttack.transform.position) >= 3  || !ToAttack.activeSelf)
        {
            State = UnitState.move;
            ToAttack = null;
            return;
        }
        //Animator.SetTrigger("DoPunching");

        if (ToAttack.GetComponent<Soldier>() != null && CanIHit)
        {
            ToAttack.GetComponent<Soldier>().Hit();
        }
        else if (ToAttack.GetComponent<Worker>() != null && CanIHit)
        {
            ToAttack.GetComponent<Worker>().Hit();
        }
        else if(ToAttack.GetComponent<Castle>() != null && CanIHit)
        {
            ToAttack.GetComponent<Castle>().Hit();
        }
        CanIHit = false;
        InAFight = false;
        State = UnitState.move;
    }
    private void Move()
    {
        if (ToAttack != null) 
        {
            State = UnitState.fight;
            return;
        }
        //Animator.ResetTrigger("DoPunching");
        var distance = Speed * Time.deltaTime;
        Vector3 PositionToGo = Vector3.MoveTowards(transform.position, new Vector3(99.5f, 2.5f, 0), distance);
        transform.position = new Vector3(PositionToGo.x, 2.5f, PositionToGo.z);
    }
}
