﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControls : MonoBehaviour {
    public GameObject Worker;
    public GameObject Soldier;
    public GameObject FlagGO;
    public Camera MainCam;
    public ObjectDB ObjectDB;
    public UpdateUI UpdateUI;
    public int ResourcesPerUnit = 5;
    private float CoolDownTimer = 0;
    private bool CanISpawn = true;
    public LayerMask clickMask;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

        CoolDownTimer -= Time.deltaTime;
        if (CoolDownTimer <= 0f)
        {
            CanISpawn = true;
            CoolDownTimer = 0.5f;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
        if (Input.GetKeyDown(KeyCode.W) && CanISpawn)
        {
            if((UpdateUI.CurrentResourcesAmount - ResourcesPerUnit) >= 0)
            {
                Vector3 SpawnPOS = new Vector3(99.5f, 1.5f, 0);
                GameObject worker = SimplePool.Spawn(Worker, SpawnPOS, Quaternion.identity);
                ObjectDB.AddWorkerToList(worker);
                UpdateUI.UpdateWorkers(1);
                UpdateUI.UseResources(5);
                CanISpawn = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.F) && CanISpawn)
        {
            if((UpdateUI.CurrentResourcesAmount - ResourcesPerUnit) >= 0)
            {
                Vector3 SpawnPOS = new Vector3(99.5f, 1.5f, 0);
                SimplePool.Spawn(Soldier, SpawnPOS, Quaternion.identity);
                UpdateUI.UpdateFighters(1);
                UpdateUI.UseResources(ResourcesPerUnit);
                CanISpawn = false;
            }

        }
        if (Input.GetButtonDown("Fire1"))
        {
            Vector3 clickPosition = Vector3.one;

            Ray ray = MainCam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit, 10000000f, clickMask))
            {
                clickPosition = hit.point;
                clickPosition.z = 0;
                clickPosition.y = 0.8f;
                FlagGO.transform.position = clickPosition;
            }
        }
	}
}
