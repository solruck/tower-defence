﻿using UnityEngine;

public class SpawnEnemy : MonoBehaviour {
    public GameObject Enemy;
    [SerializeField]
    public float spawnTimer = 0;
    public float TimeBeforeStartSpawningEnemies = 3;
    public bool LastSpawningBoolValue;
    public bool DoSpawn = true;
    private void Update()
    {
        if (this.DoSpawn != true) {
            return;
        }
        if(Time.timeSinceLevelLoad <= TimeBeforeStartSpawningEnemies) { return; }
        spawnTimer -= Time.deltaTime;
        if (spawnTimer <= 0f)
        {
            SpawnAEnemy();
            spawnTimer = Random.value*4;
        }

    }
    public void SpawnAEnemy()
    {
        GameObject enemy =  SimplePool.Spawn(Enemy, new Vector3(-99.5f, 2.5f, 0), Quaternion.identity);
        enemy.transform.Rotate(enemy.transform.rotation.x, 90, enemy.transform.rotation.z);
    }
}
